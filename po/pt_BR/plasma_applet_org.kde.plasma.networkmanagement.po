# Translation of plasma_applet_org.kde.plasma.networkmanagement.po to Brazilian Portuguese
# Copyright (C) 2014-2020 This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2014, 2015, 2019, 2020.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Frederico Gonçalves Guimarães <frederico@teia.bio.br>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:01+0000\n"
"PO-Revision-Date: 2023-03-22 10:50-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/ui/ConnectionItem.qml:60
#, kde-format
msgid "Connect"
msgstr "Conectar"

#: contents/ui/ConnectionItem.qml:60
#, kde-format
msgid "Disconnect"
msgstr "Desconectar"

#: contents/ui/ConnectionItem.qml:85
#, kde-format
msgid "Show Network's QR Code"
msgstr "Mostrar o código QR da rede"

#: contents/ui/ConnectionItem.qml:90
#, kde-format
msgid "Configure…"
msgstr "Configurar..."

#: contents/ui/ConnectionItem.qml:121
#, kde-format
msgid "Speed"
msgstr "Velocidade"

#: contents/ui/ConnectionItem.qml:126
#, kde-format
msgid "Details"
msgstr "Detalhes"

#: contents/ui/ConnectionItem.qml:169
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Current download speed is %1 kibibytes per second; current upload speed is "
"%2 kibibytes per second"
msgstr ""
"A velocidade de download atual é de %1 kibibytes por segundo; a velocidade "
"de upload atual é de %2 kibibytes por segundo"

#: contents/ui/ConnectionItem.qml:286
#, kde-format
msgid "Connected, ⬇ %1/s, ⬆ %2/s"
msgstr "Conectado, ⬇ %1/s, ⬆ %2/s"

#: contents/ui/ConnectionItem.qml:290
#, kde-format
msgid "Connected"
msgstr "Conectado"

#: contents/ui/DetailsText.qml:45
#, kde-format
msgid "Copy"
msgstr "Copiar"

#: contents/ui/main.qml:24
#, kde-format
msgid "Networks"
msgstr "Redes"

#: contents/ui/main.qml:33
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn off Airplane Mode"
msgstr "Clique do meio para desligar o modo avião"

#: contents/ui/main.qml:35
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn on Airplane Mode"
msgstr "Clique do meio para ligar o modo avião"

#: contents/ui/main.qml:79 contents/ui/Toolbar.qml:69
#, kde-format
msgid "Enable Wi-Fi"
msgstr "Habilitar Wi-Fi"

#: contents/ui/main.qml:87
#, kde-format
msgid "Enable Mobile Network"
msgstr "Habilitar rede móvel"

#: contents/ui/main.qml:95
#, kde-format
msgid "Enable Airplane Mode"
msgstr "Habilitar modo avião"

#: contents/ui/main.qml:101
#, kde-format
msgid "Open Network Login Page…"
msgstr "Abrir página de autenticação da rede..."

#: contents/ui/main.qml:106
#, kde-format
msgid "&Configure Network Connections…"
msgstr "&Configurar as conexões de rede..."

#: contents/ui/main.qml:143
#, kde-format
msgctxt "@title:window"
msgid "QR Code for %1"
msgstr "Código QR para %1"

#: contents/ui/PasswordField.qml:16
#, kde-format
msgid "Password…"
msgstr "Senha..."

#: contents/ui/PopupDialog.qml:102
#, kde-format
msgid "Airplane mode is enabled"
msgstr "O modo avião está habilitado"

#: contents/ui/PopupDialog.qml:106
#, kde-format
msgid "Wireless and mobile networks are deactivated"
msgstr "As redes móveis e sem fio estão desativadas"

#: contents/ui/PopupDialog.qml:108
#, kde-format
msgid "Wireless is deactivated"
msgstr "A rede sem fio está desativada"

#: contents/ui/PopupDialog.qml:111
#, kde-format
msgid "Mobile network is deactivated"
msgstr "A rede móvel está desativada"

#: contents/ui/PopupDialog.qml:114
#, kde-format
msgid "No matches"
msgstr "Nenhuma ocorrência"

#: contents/ui/PopupDialog.qml:116
#, kde-format
msgid "No available connections"
msgstr "Nenhuma conexão disponível"

#: contents/ui/Toolbar.qml:118
#, kde-format
msgid "Enable mobile network"
msgstr "Habilitar rede móvel"

#: contents/ui/Toolbar.qml:143
#, kde-kuit-format
msgctxt "@info"
msgid "Disable airplane mode<nl/><nl/>This will enable Wi-Fi and Bluetooth"
msgstr "Desabilitar modo avião<nl/><nl/>Isto irá habilitar o Wi-Fi e Bluetooth"

#: contents/ui/Toolbar.qml:144
#, kde-kuit-format
msgctxt "@info"
msgid "Enable airplane mode<nl/><nl/>This will disable Wi-Fi and Bluetooth"
msgstr "Habilitar modo avião<nl/><nl/>Isto irá desabilitar o Wi-Fi e Bluetooth"

#: contents/ui/Toolbar.qml:154
#, kde-format
msgid "Hotspot"
msgstr "Ponto de acesso"

#: contents/ui/Toolbar.qml:178 contents/ui/Toolbar.qml:189
#, kde-format
msgid "Disable Hotspot"
msgstr "Desativar ponto de acesso"

#: contents/ui/Toolbar.qml:183 contents/ui/Toolbar.qml:189
#, kde-format
msgid "Create Hotspot"
msgstr "Criar ponto de acesso"

#: contents/ui/Toolbar.qml:221
#, kde-format
msgid "Configure network connections…"
msgstr "Configurar as conexões de rede..."

#: contents/ui/TrafficMonitor.qml:36 contents/ui/TrafficMonitor.qml:106
#, kde-format
msgid "/s"
msgstr "/s"

# Não traduzir (Alvarenga)
#: contents/ui/TrafficMonitor.qml:88
#, kde-format
msgid "Upload"
msgstr "Upload"

# Não traduzir (Alvarenga)
#: contents/ui/TrafficMonitor.qml:88
#, kde-format
msgid "Download"
msgstr "Download"

#, fuzzy
#~| msgid "Connect"
#~ msgctxt "@info:tooltip %1 is the name of a network connection"
#~ msgid "Connect to %1"
#~ msgstr "Conectar"

#, fuzzy
#~| msgid "Disconnect"
#~ msgctxt "@info:tooltip %1 is the name of a network connection"
#~ msgid "Disconnect from %1"
#~ msgstr "Desconectar"

#~ msgctxt "text field placeholder text"
#~ msgid "Search…"
#~ msgstr "Pesquisar..."

#~ msgctxt "button tooltip"
#~ msgid "Search the connections"
#~ msgstr "Pesquisar conexões"

#~ msgid ""
#~ "Connected, <font color='%1'>⬇</font> %2/s, <font color='%3'>⬆</font> %4/s"
#~ msgstr ""
#~ "Conectado, <font color='%1'>⬇</font> %2/s, <font color='%3'>⬆</font> %4/s"

#~ msgid "Enable wireless"
#~ msgstr "Habilitar rede sem fio"

#~ msgid "Available connections"
#~ msgstr "Conexões disponíveis"
